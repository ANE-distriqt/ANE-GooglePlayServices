

###### 2016.03.02

Updated to v8487000



###### 2015.11.18

Update to latest Google Services lib


###### 2015.11.18

Update to latest Google Services lib


###### 2015.11.05

Updated to latest SDK


###### 2015.06.15

Removed debug code from AS lib
iOS: Updated to latest common lib
Android: Windows: Fix for bug in AIR packager resulting in missing resources


###### 2015.04.27

Android: Updated to version 7095000


###### 2015.02.23

Android: Added the android support v4 library
